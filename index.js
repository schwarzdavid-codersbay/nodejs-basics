const path = require('path');
const express = require("express");
const router = require("./router");
const app = express();

const users = [];

// Middleware führt noch etwas aus
// In diesem Fall wandelt er JSON in tatsächliche js-objekte um und entziffert zb "%C3" zu einem "Ü"
app.use(express.json());
app.use(express.urlencoded({extended: true}));
 
app.get('/', function(req, res) {
	console.log("tralalala");
	
	// Sendet text zurück
	res.send("Hallo");
});
 
// Get soll irgendwas ausgeben
app.get('/user', function(req, res) {
	// Json liefert maschinell lesbare dateien zurück
	res.json(users);
});

// Post fügt neuen Eintrag hinzu
app.post('/user', function(req, res) {
	users.push(req.body);
	res.status(201).send("User created");
});

// Put verändert/bearbeitet bestehenden eintrag
app.put('/user/:index', (req, res) => {
	// parseInt versucht aus einem string eine zahl zu machen. Aus "0" wird also 0
	if (isNaN(parseInt(req.params.index)) || req.params.index < 0 || req.params.index >= users.length) {
		res.status(404).send("user not found");
	} else {
		users[parseInt(req.params.index)] = req.body;
		res.send("Done");
	}
});

// Delete löscht eintrag
app.delete('/user/:index', (req, res) => {
	if (isNaN(parseInt(req.params.index)) || req.params.index < 0 || req.params.index >= users.length) {
		res.status(404).send("user not found");
	} else {
		users.splice(parseInt(req.params.index), 1);
		res.send("Deleted");
	}
});
 
app.listen(3000, (err) => {
	if(err) {
		console.error(err);
	} else {
		console.log("Server running");
	}
});